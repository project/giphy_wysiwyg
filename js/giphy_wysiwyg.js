/**
 * @file
 * giphy_wysiwyg js file.
 */

 (function ($, Drupal) {

  'use strict';

  /**
   * Process add_gif_url.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.add_gif_url = {
    attach: function (context, settings) {

      if(context.action == "https://localhost/giphy-wysiwyg/dialog/full_html"){
        $(document).on("click",".ul_gif_list li img",function(ev){
          ev.stopPropagation();
          ev.preventDefault();
          $('.image_url').val($(this).attr('src'));
        });
      }      
    }
  };
})(jQuery, Drupal);