<?php

namespace Drupal\giphy_wysiwyg\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * The media_entity plugin for giphy
 *
 * @CKEditorPlugin(
 *   id = "giphy_wysiwyg",
 *   label = @Translation("Giphy Wysiwyg"),
 *   module = "giphy_wysiwyg"
 * )
 */
class GiphyWysiwyg extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'giphy_wysiwyg') . '/plugin/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'giphy_wysiwyg' => [
        'label' => $this->t('Giphy Wysiwyg'),
        'image' => drupal_get_path('module', 'giphy_wysiwyg') . '/plugin/icon.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $editor_settings = $editor->getSettings();
    $plugin_settings = NestedArray::getValue($editor_settings, [
      'plugins',
      'defaults',
      'children',
    ]);
    $settings = $plugin_settings ?: [];

    $form['defaults'] = [
      '#title' => $this->t('Default Settings'),
      '#type' => 'fieldset',
      '#tree' => TRUE
    ];
    return $form;
  }
}
