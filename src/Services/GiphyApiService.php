<?php

namespace Drupal\giphy_wysiwyg\Services;

use GuzzleHttp\Client;

class GiphyApiService {

  public function getListGifs($words){

    $client = \Drupal::httpClient();
    $request = $client->get('https://api.giphy.com/v1/gifs/search?api_key=RspRREYn7COdmVitbKEOuNhvKptu87p1&q='.$words.'&limit=25&offset=0&rating=g&lang=en');
    $data = json_decode($request->getBody());
    foreach($data->data as $key => $dataImage){
      $response[$key] = strtok($dataImage->images->original->url, '?');
    }
    return $response;
  }
}