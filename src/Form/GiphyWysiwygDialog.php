<?php

namespace Drupal\giphy_wysiwyg\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\DataCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\giphy_wysiwyg\Services\GiphyApiService;

/**
 * A class for a Gif embed dialog.
 */
class GiphyWysiwygDialog extends FormBase {


  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

    /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $giphyApiService;

  /**
   * GiphyWysiwygDialog constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer, GiphyApiService $giphy_api_service) {
    $this->render = $renderer;
    $this->giphyApiService = $giphy_api_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('giphy_wysiwyg.giphy_api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {
    // Add AJAX support.
    $form['#prefix'] = '<div id="giphy-wysiwyg-dialog-form">';
    $form['#suffix'] = '</div>';
    // Ensure relevant dialog libraries are attached.
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    
    $form['giphy_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Giphy text'),
      '#default_value' => $this->getUserInput($form_state, 'giphy_text'),
      '#attributes' => array('class' => array('giphy_text')),
      '#ajax' => [
        'callback' => '::ajaxGiphyText',
        'event' => 'keyup',
        'wrapper' => 'giphy-wysiwyg-dialog-form',
      ],
    ];

    $form['gif_list'] = [
      '#type' => 'markup',
      '#markup' => '<div id="gif_list"></div>',
    ];

    $form['image_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image URL'),
      '#required' => TRUE,
      '#default_value' => $this->getUserInput($form_state, 'image_url'),
      '#attributes' => array('class' => array('image_url')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => [],
      '#ajax' => [
        'callback' => '::ajaxSubmit',
        'event' => 'click',
        'wrapper' => 'giphy-wysiwyg-dialog-form',
      ],
    ];
    $form['#attached']['library'][] = 'giphy_wysiwyg/giphy_wysiwyg';
    return $form;
  }

 /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

 /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The AJAX commands were already added in the AJAX callback. Do nothing in
    // the submit form.
  }
  /**
   * Get a value from the widget in the WYSIWYG.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state to extract values from.
   * @param string $key
   *   The key to get from the selected WYSIWYG element.
   *
   * @return string
   *   The default value.
   */
  protected function getUserInput(FormStateInterface $form_state, $key) {
    return isset($form_state->getUserInput()['editor_object'][$key]) ? $form_state->getUserInput()['editor_object'][$key] : '';
  }

  /**
   * An AJAX submit callback to validate the WYSIWYG modal.
   */
  public function ajaxGiphyText(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$form_state->getErrors()) {
      // Load the provider and get the information needed for the client.
      $response->addCommand(new DataCommand('.form-item-giphy-text', 'list_gifs', $this->getGiphyValues($form_state)));
    }
    else {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand(NULL, $form));
    }
    return $response;
  }

  
  /**
   * Get the values from the form and provider required for the client.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state from the dialog submission.
   *   The provider loaded from the user input.
   *
   * @return array
   *   An array of values sent to the client for use in the WYSIWYG.
   */
  protected function getGiphyValues(FormStateInterface $form_state) {

    $gifs_list = $this->giphyApiService->getListGifs($form_state->getValue("giphy_text"));

    return [
      'gifs_list' => $gifs_list
    ];
  }
 

  /**
   * An AJAX submit callback to validate the WYSIWYG modal.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (!$form_state->getErrors()) {
      // Load the provider and get the information needed for the client.
      $response->addCommand(new EditorDialogSave($this->getClientValues($form_state)));
      $response->addCommand(new CloseModalDialogCommand());
    }
    else {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand(NULL, $form));
    }
    return $response;
  }
  
  /**
   * Get the values from the form and provider required for the client.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state from the dialog submission.
   *   The provider loaded from the user input.
   *
   * @return array
   *   An array of values sent to the client for use in the WYSIWYG.
   */
  protected function getClientValues(FormStateInterface $form_state) {
    return [
      'image_url' => $form_state->getValue("image_url")
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'giphy_wysiwyg_dialog';
  }
}
