/**
 * @file
 * The JavaScript file for the wysiwyg integration.
 */

(function ($) {

  /**
   * A CKEditor plugin for vido embeds.
   */
  CKEDITOR.plugins.add('giphy_wysiwyg', {

    /**
     * Set the plugin modes.
     */
    modes: {
      wysiwyg: 1
    },

    /**
     * Define the plugin requirements.
     */
    requires: 'widget',

    /**
     * Allow undo actions.
     */
    canUndo: true,

    /**
     * Init the plugin.
     */
    init: function (editor) {
      this.registerWidget(editor);
      this.addCommand(editor);
      this.addIcon(editor);
    },

    /**
     * Add the command to the editor.
     */
    addCommand: function (editor) {
      var self = this;
      var modalSaveWrapper = function (values) {
        self.modalSave(editor, values);
      };
      editor.addCommand('giphy_wysiwyg', {
        exec: function (editor, data) {
          // If the selected element while we click the button is an instance
          // of the video_embed widget, extract it's values so they can be
          // sent to the server to prime the configuration form.
          var existingValues = {};
          if (editor.widgets.focused && editor.widgets.focused.name == 'giphy_wysiwyg') {
            existingValues = editor.widgets.focused.data.json;
          }
          Drupal.ckeditor.openDialog(editor, Drupal.url('giphy-wysiwyg/dialog/' + editor.config.drupal.format), existingValues, modalSaveWrapper, {
            title: Drupal.t('Giphy'),
            dialogClass: 'giphy-wysiwyg-dialog'
          });
        }
      });
    },

    /**
     * A callback that is triggered when the modal is saved.
     */
    modalSave: function (editor, values) {
      // Insert a video widget that understands how to manage a JSON encoded
      // object, provided the video_embed property is set.
      var widget = editor.document.createElement('p');
      widget.setHtml('<img src="'+values["image_url"]+'" />');
      editor.insertHtml(widget.getOuterHtml());
    },

    /**
     * Register the widget.
     */
    registerWidget: function (editor) {
      var self = this;
      editor.widgets.add('giphy_wysiwyg', {
        downcast: self.downcast,
        upcast: self.upcast,
        mask: true
      });
    },

    /**
     * Check if the element is an instance of the video widget.
     */
    upcast: function (element, data) {
      // Upcast check must be sensitive to both HTML encoded and plain text.
      if (!element.getHtml().match(/^({(?=.*image_url\b)(.*)})$/)) {
        return;
      }
      data.json = JSON.parse(element.getHtml());
      element.setHtml(Drupal.theme('GiphyWidget', data.json));
      return element;
    },

    /**
     * Turns a transformed widget into the downcasted representation.
     */
    downcast: function (element) {
      element.setHtml(JSON.stringify(this.data.json));
    },

    /**
     * Add the icon to the toolbar.
     */
    addIcon: function (editor) {
      if (!editor.ui.addButton) {
        return;
      }
      editor.ui.addButton('giphy_wysiwyg', {
        label: Drupal.t('Giphy'),
        command: 'giphy_wysiwyg',
        icon: this.path + '/icon.png'
      });
    }
  });

  /**
   * The widget template viewable in the WYSIWYG after creating a video.
   */
  Drupal.theme.GiphyWidget = function (settings) {
    return [
      '<span class="giphy-widget">',
        '<img class="giphy-widget__image" src="">',
        '<span class="giphy-widget__summary">',
        '</span>',
      '</span>'
    ].join('');
  };

  // $(document).once().ajaxComplete(function(event, xhr, settings) {
  //   if(typeof settings.extraData !== 'undefined' && settings.extraData._triggering_element_name == 'giphy_text'){
  //     let data = jQuery.parseJSON(xhr.responseText)[1].value["gifs_list"];
  //     let listGifs = '';

  //     listGifs += '<ul class="ul_gif_list">';
  //     data.forEach( function(valor, indice, array) {
  //       listGifs += '<li><img src="'+valor+'"/></li>';
  //     });
  //     listGifs += '</ul>';

      // $('#gif_list').html(listGifs);
      
      // $('.giphy_text').focus();    
    // }    
  // });
})(jQuery);
