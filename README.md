# Giphy Wysiwyg

This Drupal module integrates a Giphy plugin into CKEditor, allowing users to easily search for and insert GIFs into their content.
Enhance user engagement and expression with dynamic visual elements!

**Features**
Basic functionality: Integrates Giphy plugin into CKEditor.
Unique features: Enables seamless GIF search and insertion directly within the content editor.
Use cases: Ideal for content creators seeking to enrich their Drupal site with visually engaging GIFs. Enhance user interaction and storytelling.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/giphy_wysiwyg).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/giphy_wysiwyg).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This project requires Drupal core and CKEditor. No additional dependencies are needed.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After installation, users can access the Giphy plugin directly within CKEditor.
No additional configuration required. Simply click the Giphy button in the editor to access the GIF search functionality.


## Maintainers

- Carlos Romero - [carlos-romero](https://www.drupal.org/u/carlos-romero)